package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.games.Games;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GamesControllerTest {
    @Autowired
    private MockMvc mockMvc;


    // story 1
    @Test
    void should_test_game_have_been_created_successfully() throws Exception {
        mockMvc.perform(post("/api/games"))
        .andExpect(status().isCreated());
    }

    @Test
    void should_return_games_address() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(header().string("location", "/api/games/1"));
    }

    // story 2
    @Test
    void should_return_content_of_game_by_id() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        String content = mvcResult.getResponse().getContentAsString();
        Games games = objectMapper.readValue(content, Games.class);
        assertEquals(4, games.getAnswer().length());
    }

    // story 3
    @Test
    void should_return_status_404_when_game_id_not_exists() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(get("/api/games/1"));
        mockMvc.perform(patch("/api/games/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"answer\": \"1234\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_status_400_when_answer_format_is_not_correct() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(get("/api/games/1"));
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"answer\": \"123a\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_users_answer() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(get("/api/games/1"));
        mockMvc.perform(patch("/api/games/1")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"answer\": \"1234\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }
}
