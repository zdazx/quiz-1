package com.twuc.webApp.games;

import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class GameFactory {
    private HashMap<Integer, Games> games;
    private Integer gameId;

    public GameFactory() {
        this.games = new HashMap<>();
    }

    public Integer getGameId() {
        return this.gameId;
    }

    public HashMap<Integer, Games> getGames() {
        return this.games;
    }

    public void createGame(){
        Games game = new Games();
        game.create();
        this.gameId = game.getGameId();
        games.put(this.gameId, game);
    }
}
