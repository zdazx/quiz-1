package com.twuc.webApp.games;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

public class Games {
    @JsonProperty("id")
    private Integer gameId;
    private String answer;

    public Games() {
        this.gameId = 0;
        this.answer = "";
    }

    public String getAnswer() {
        return answer;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void create(){
        this.gameId++;
        this.answer = "";
        Set<Integer> set = new HashSet<>();
        while (set.size() < 4){
            int num = (int) (Math.random() * 10);
            set.add(Integer.valueOf(num));
        }
        this.answer = set.stream().map(i -> i.toString()).reduce("", String::concat);
    }
}
