package com.twuc.webApp.controller;

import com.twuc.webApp.games.GameFactory;
import com.twuc.webApp.games.Games;
import com.twuc.webApp.games.GuessResult;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

@RestController
public class GamesController {

    private AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext("com.twuc.webApp.games");

    @PostMapping("/api/games")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Games> returnGameAddress(){
        GameFactory factory = context.getBean(GameFactory.class);
        factory.createGame();
        return ResponseEntity.created(null)
                .location(URI.create("/api/games/" + factory.getGameId()))
                .build();
    }

    @GetMapping("/api/games/{id}")
    public ResponseEntity returnGameByGameId(@PathVariable("id") Integer gameId){
        GameFactory factory = context.getBean(GameFactory.class);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body("{\"id\":"+gameId+",\"answer\":\""+factory.getGames().get(gameId).getAnswer()+"\"}");
    }

    @PatchMapping("/api/games/{gameId}")
    public ResponseEntity returnResultOfVerify(@PathVariable Integer gameId, @RequestBody String answer){
        GameFactory factory = context.getBean(GameFactory.class);
        // 如果给定游戏的 gameId 并不存在则其响应的 status code 应当为 404。
        if (!(factory.getGames().containsKey(gameId))) {
            return ResponseEntity.status(404).build();
        }
        // 如果给定游戏的 gameId 的格式不正确（只能够为数字）则响应的 status code 应当为 400。
        if (!("" + gameId).matches("[0-9]+")){
            return ResponseEntity.status(400).build();
        }
        // 如果 answer 的字符串格式不正确则响应的 status code 应当为 400。
        // 其中包括字符串的长度不为 4，字符串中包含非数字字符以及字符串的字符含有重复数字的情况。
        Set<Integer> set = new HashSet<>();
        int indexOfStart = answer.indexOf("answer");
        String answerOfNum = answer.substring(indexOfStart + 10,indexOfStart + 14);
        for (int i = 0; i < 4; i++) {
            Integer item = Integer.valueOf(answerOfNum.charAt(i));
            set.add(item);
        }
        if (!answerOfNum.matches("[0-9]{4}") || set.size()!= 4){
            return ResponseEntity.status(400).build();
        }
        String result = judgeAnswer(gameId, answerOfNum);
        GuessResult guessResult = new GuessResult();
        guessResult.setHint(result);
        if (result.equals("4A0B")){
            guessResult.setCorrect(true);
            return ResponseEntity.status(200)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(guessResult);
        }
        guessResult.setCorrect(false);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(guessResult);
    }

    private String judgeAnswer(Integer gameId, String answerOfUser){
        GameFactory factory = context.getBean(GameFactory.class);
        Games games = factory.getGames().get(gameId);
        String answer = games.getAnswer();
        int numA = 0;
        int numB = 0;
        if (answer == answerOfUser){
            return "4A0B";
        }else {
            for (int i = 0; i < 4; i++) {
                int index = answer.indexOf(answerOfUser.charAt(i));
                if (index != -1) {
                    if (index == i) {
                        numA++;
                    }else {
                        numB++;
                    }
                }
            }
        }
        return numA + "A" + numB + "B";
    }
}
